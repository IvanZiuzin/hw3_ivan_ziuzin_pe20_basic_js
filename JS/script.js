"use strict"
// Этот код работает в современном режиме


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ  ------------------------

// Task #1 --------------------------------------

// Реализовать программу на JavaScript, которая будет находить все числа, кратные 5(деление на 5 без остатка) в заданном диапазоне
// Задание выполняетна на чистом JavaScript без использования библиотек типа jQuery / React.

// Технические требования к заданию:
// Получить из модального окна браузера число, которое введет пользователь 
// Вывести в консоли все числа, кратные 5, от 0 до числа, введенного пользователем.
// Если таких чисел нет  - вывести в консоль фразу "Sorry, no numbers"
// Обязательно используем синтаксис ES6 (ES2015) для создания переменных

// Task #2 --------------------------------------

// --- Необязательные задания повышенной сложности:

// 1/ Проверить, - является ли введенное значние - целым числом.
// Если это условаие не выполняется - повторять выведение на экран, пока не будет введено целое число
// 2/ получить 2 числа: "m" и "n".
// Вывести в консоль все простые числа - (https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
// в диапазоне: от "m" до "n"
// (меншее из введённых чисел будет "m", большее - будет "n")
// Если хотябы одно из чисел не соотвествует условиям валидности, указанным выше, - 
// вывести уведомление про ошибку и зпросить оба числа снова

//---**************************----
// Простые числа - это натуральные числа, больше 1, которые имеют только два делителя: 
// 1 и само это число. Например, числа 2, 3, 5, 7, 11, 13 и 17 являются простыми числами.
// Простые числа имеют много интересных свойств и являются важными для многих областей математики, 
// включая криптографию и теорию чисел. Их использование может обеспечить надежность в криптографии 
// и защитить данные от взлома.
// Ноль не удовлетворяет этому определению, 
// потому что он не является натуральным числом и имеет бесконечное количество делителей.
//---**************************----


// ----РЕШЕНИЕ ЗАДАЧИ: -----

// Task #1 ----- < Integer numbers - multiple of 5 > --------------------------------
console.log('Task #1 < Integer numbers - multiple of 5 > started --- ');

// 1. ПРОВЕРКА ВВЕДЕННОГО ПОЛЬЗОВАТЕЛЕМ ЗНАЧЕНИЯ:
// Переменные:
let userNumber
let userNumberCheck
// Цикл проверки - является ли введенное число целым:
while (!Number.isInteger(userNumber) || userNumber.length === 0) {
    userNumber = parseFloat(prompt('Task#1\nHi,\nplease enter any number you want','Use only figures'));
    userNumberCheck = Number.isInteger(userNumber);
}
console.log(typeof userNumber, userNumber, ' - this is userNumber');
console.log(typeof userNumberCheck, userNumberCheck,' - this is userNumberCheck result');

// 2. ЦИКЛ ДЛЯ ВЫВОДА В КОНСОЛЬ ЧИСЕЛ, КРАТНЫХ 5 ---------------------------------
// Старт задачи:
console.log(`> The list of numbers multiple of 5 between "0" and ${userNumber}:`);
// Переменные:
let consolNumber // число для вывода в консоль
let invalidNumberCheck = false; // проверка на соотвествие условию, по умлочанию false
let massageForUser = "Sorry, no numbers!"; // сообщение для пользователя 
// Цикл для выводы чисел, кратных 5:
for (consolNumber = 0; consolNumber <= userNumber; consolNumber++){
    if(consolNumber % 5 === 0 && consolNumber !==0){
        console.log(typeof consolNumber, consolNumber);
        invalidNumberCheck = true;
        } 
}
// Проверка на наличие чисел, не кратных 5:
if(!invalidNumberCheck){
    console.log(massageForUser);
    alert (massageForUser);
}

console.log('Task #1 comleted --- ');
// задача завершена -------------------


// Task #2 ---- < Prime numbers > ----------------------------------

// Логика простое число:
// Условие 1: это натуральное число (отрицательные и нецелые (дробные) числа к натуральным не относятся);
// Условие 2: только > 0;
// Условие 3: не может быть нулем !== 0;
// Условие 4: только целое число;
// Условие 5: это число > 1;
// Условие 6: это число, у которого всего два делителя: 1 и само число;
// Условие 7: это число, которое нельзя получить произведением 2х меньших натуральных чисел; 

// Т.е. чтобы число из диапазона считалось простым надо, чтобы :
//  - не делится на любое из чисел, которые находятся до него в ряду 
//  - если число имеет больше 2-х делителей - оно не простое
//  - нельзя получить путем умножения двух меньших натуральных чисел.
//  - Все остальные непростые натуральные числа, большие 1, называются составными числами.

// Разделим задачу на 2 этапа: 
// - А)  При получении от пользоватлеля чисел, отсечём на этапе ввода отрицательные и ненатуральные числа
//     - условия 1, 2, 3, 4
// - Б)  Сделаем проверку на наличие простых чичел в заданном диапазоне 
//     - условия 5, 6, 7

console.log('Task #2 < Prime numbers > started --- ');

// Часть А) ----- условия 1, 2, 3, 4--
// Переменные: 
let m // Первое число 
let n // Второе число 
let mCheck // Проверка m на целое число 
let nCheck // Проверка n на целое число 

// Цикл проверки вводных чисел (пользователь должен ввесли два числа так чтобы n > m):
Openingcheck:
while (!Number.isInteger(m) || !Number.isInteger(n) || n <= m || m === 0 || m < 0 || n === 0 || n < 0 || n.length === 0|| m.length === 0) {
    alert ('You need to enter a couple of numbers:\n1 -> each number sould not be fractional number\n2 -> each number should > 0 \n3 -> each number should not be 0 or Null(empty)\n4 -> first number sould be > than second number');
    m = parseFloat(prompt('Please enter first number you want','use only figures!'));
    n = parseFloat(prompt('Please enter second number\nnotice:\nthis number should be higher than previous one','use only figures!'));  
    mCheck = Number.isInteger(m);
    nCheck = Number.isInteger(n);
    if(!Number.isInteger(m) || !Number.isInteger(n) || n <= m || m === 0 || m < 0 || n === 0 || n < 0 ) {
        alert('Soryy, error!\nRead carefully the conditions - which numbers should be entered\nTry enter another couple of numbers!')
    }
    if(!Number.isInteger(m) || !Number.isInteger(n) || n <= m || m === 0 || m < 0 || n === 0 || n < 0 || n.length === 0|| m.length === 0) continue Openingcheck;
}
console.log(typeof m, m, 'this is first number - m');
console.log(typeof n, n,'this is second number - n');
console.log(`> The list of prime numbers between ${m} and ${n}:`);

// Часть Б)------ условия 5,6,7--
// Переменные:
let primeNum // искомое простое число 
let maxLimit // ограничитель диапазона в котром выбираем простые числа  
// Цикл для проверки наличия простых чисел и вывода их в консоль

// ФУНКЦИЯ №1 
// проверяет является ли число простым (для любого числа, которое будет передано в неё в качества аргумента)
function PrimeNumCheck (a){ 
    maxLimit = Math.sqrt(a)
    for(let i = 2; i <= maxLimit; i++){
        if(a % i === 0) {
            return false;
        }
    }
    return a > 1; // функция возвращает true только если число больше единицы
}


// ФУНКЦИЯ №2 
// отбирает простые числа из диапазона и выводит в консоль

function PrimeNumSelect (start, end){
    for(primeNum = start; primeNum <= end; primeNum++){ // прогоняет весь заданный пользователем диапазон чисел
        if(PrimeNumCheck (primeNum)){
            console.log(primeNum);
        }
    }  
} 

// Вариант с массивом

// function PrimeNumSelect (start, end){
//     const primeNymbs = [];
//     for(primeNum = start; primeNum <= end; primeNum++){ // прогоняет весь заданный пользователем диапазон чисел
//             if(PrimeNumCheck (primeNum)){
//                 primeNymbs.push(primeNum);
//             }
//     }
//     return primeNymbs;
   
// } 

console.log(PrimeNumSelect(m, n));
console.log('Task #2 comleted --- ');




// ЗАМЕТКИ ДЛЯ МЕНЯ:
// Функции преобразования: 

// Number.isInteger()------------------------
// Если целевое значение является целым числом, возвращает true. 
// Если значение NaN или Infinity, то возвращает false.


// Number.isNaN(value)------------------------
// проверяет, является ли переданное значение NaN
// Number. isNaN() не преобразует значения в число и не возвращает true для 
// любого значения, которое не относится к типу Number.

// Number.isNaN(NaN); // true
// Number.isNaN(Number.NaN); // true
// Number.isNaN(0 / 0) // true
// При использовании глобальной функции isNaN() это всё будет true
// Number.isNaN('NaN');      // false
// Number.isNaN(undefined);  // false
// Number.isNaN({});         // false
// Number.isNaN('blabla');   // false

// А это всё в любом случае будет false
// Number.isNaN(true);
// Number.isNaN(null);
// Number.isNaN(37);
// Number.isNaN('37');
// Number.isNaN('37.37');
// Number.isNaN('');
// Number.isNaN(' ');

// ------
// функция «isNaN()» при проверке вернет «true» в двух случаях: если при проверке 
// значение уже NaN и если оно станет NaN после попытки преобразования его в число; 
// метод «Number. isNaN()» вернет «true» только если значение при проверке уже является NaN.
// isNaN(value)  - проверка на числовое значение: - "работает навыворот": NaN - true / other - false 
// isNaN() преобразует тестируемое значение в число, а затем проверяет его. 
// возвращает true для любого значения, которое не относится к типу Number.

// isNaN(NaN);       // true
// isNaN(undefined); // true
// isNaN(true);      // false
// isNaN(null);      // false
// isNaN(37);        // false
// strings
// isNaN("37");      // false: "37" преобразуется в число 37 которое не NaN
// isNaN("37.37");   // false: "37.37" преобразуется в число 37.37 которое не NaN
// isNaN("");        // false: пустая строка преобразуется в 0 которое не NaN
// isNaN(" ");       // false: строка с пробелом преобразуется в 0 которое не NaN
// isNaN("37,5");    // true
// ------

// Number(value) - преобразование в число: или число или NaN: 
// (undefined, string  - NaN) или (null - 0; true / false  - 1 / 0)

// Логическое: 
// - Значения «пустые», вроде 0, пустой строки, null, undefined и NaN, становятся false.
// - Все остальные значения становятся true.

